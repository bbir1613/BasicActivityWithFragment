package com.example.bogdan.eventbasicactivitywithfragment;

import android.app.Application;
import android.util.Log;

public class EventApplication extends Application {//add android:name=".EventApplication" in manifest
    public final static String TAG = EventApplication.class.getCanonicalName();
    private EventManager mEventManager;

    @Override
    public void onCreate() {
        super.onCreate();
        mEventManager = new EventManager();
        Log.d(TAG, "onCreate");
    }

    public EventManager getEventManager() {
        return mEventManager;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        Log.d(TAG, "onTerminate");
    }
}
