package com.example.bogdan.eventbasicactivitywithfragment;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class EventListAdapter extends BaseAdapter {
    public final static String TAG = EventListAdapter.class.getCanonicalName();
    private final Context mContext;
    private final EventManager mEventManager;
    private final List<Event> mEvents;

    public EventListAdapter(Context context, EventManager eventManager) {
        mContext = context;
        mEventManager = eventManager;
        mEvents = eventManager.getEvents();
        eventManager.setOnUpdate(this::notifyDataSetChanged);
    }

    @Override
    public int getCount() {
        return mEvents.size();
    }

    @Override
    public Object getItem(int position) {
        return mEvents.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View eventLayout = LayoutInflater.from(mContext).inflate(R.layout.event_detail, null);
        ((TextView) eventLayout.findViewById(R.id.event_title)).setText(mEvents.get(position).getTitle());
        Log.d(TAG, "getView " + position);
        return eventLayout;
    }
}
