package com.example.bogdan.eventbasicactivitywithfragment;


public class Event {
    String mTitle;

    public Event(String title) {
        this.mTitle = title;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }
}
