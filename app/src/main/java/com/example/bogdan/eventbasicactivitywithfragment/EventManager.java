package com.example.bogdan.eventbasicactivitywithfragment;


import java.util.ArrayList;
import java.util.List;

public class EventManager {
    private final List<Event> mEvents;
    private OnUpdateListener mOnUpdateListener;

    public EventManager() {
        mEvents = new ArrayList<>();
        for (int i = 0; i < 10; ++i) {
            mEvents.add(new Event("Event" + i));
        }
    }

    public List<Event> getEvents() {
        return this.mEvents;
    }

    public void addEvent() {
        mEvents.add(new Event("Added event " + mEvents.size()));
        if (mOnUpdateListener != null) mOnUpdateListener.update();
    }

    public void setOnUpdate(OnUpdateListener onUpdate) {
        this.mOnUpdateListener = onUpdate;
    }

    public interface OnUpdateListener {
        void update();
    }
}
